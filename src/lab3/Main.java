package lab3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

class Main {
	SoftwareFrame frame;
	str st;
	ActionListener list;

	public Main() {
		st = new str();
		frame = new SoftwareFrame();
		list = new WordListener();
		frame.setListener(list);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	class WordListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			String str1 = frame.text.getText();
			String str2 = frame.text1.getText();

			frame.area.setText(st.generateNgram(str1, str2));

		}
	}

	public static void main(String[] args) {
		new Main();
	}
}
